import React, {useEffect, useState} from 'react';
import * as Constants from '../constants/MazeConstants';
import {Panel, Div, Text} from '@vkontakte/vkui';
import {Colors} from "../constants/Colors";


const Maze = ({id, position, gyroscopeData}) => {
  const [currentX, setCurrentX] = useState(Constants.MAP_WIDTH / 2 - 3 *Constants.PLAYER_SIZE)
  const [currentY, setCurrentY] = useState(Constants.MAP_HEIGHT / 2)

  const canMove = (x, y) => {
    const walls = getMazeWalls()

    if (x < 0 || x >= Constants.MAP_WIDTH || y < 0 || y >= Constants.MAP_HEIGHT){
      return false;
    }

    let base = [x, y]
    let movedX = [x + Constants.PLAYER_SIZE, y]
    let movedY = [x, y + Constants.PLAYER_SIZE]
    let movedXY = [x + Constants.PLAYER_SIZE, y + Constants.PLAYER_SIZE]

    for (let i = 0; i < walls.length; i++) {
      let w = walls[i]
      if (contains(w, base) || contains(w, movedX) || contains(w, movedY) || contains(w, movedXY))
        return false
    }
    return true
  }

  const movePlayer = (deltaX, deltaY) => {
    let x = currentX
    let y = currentY

    x = x ? x : Constants.MAP_WIDTH / 2;
    y = y ? y : Constants.MAP_HEIGHT / 2;
    deltaX = deltaX ? deltaX : 0;
    deltaY = deltaY ? deltaY : 0;

    const applyX = canMove(x + deltaX, y) ? deltaX : -deltaX;
    const applyY =  canMove(x,y + deltaY) ? deltaY : -deltaY;

    setCurrentX(oldX => oldX + applyX);
    setCurrentY(oldY => oldY + applyY);
  }

  const contains = (wall, position) => {
    return wall[0] <= position[0] && position[0] <= wall[0] + Constants.CELL_WIDTH &&
      wall[1] <= position[1] && position[1] <= wall[1] + Constants.CELL_HEIGHT
  }

  const getMazeWalls = () => {
    let mazeWalls = []
    for (let i = 0; i < Constants.CELL_COUNT; i++) {
      for (let j = 0; j < Constants.CELL_COUNT; j++) {
        if (Constants.MAZE[i][j] === '0')
          mazeWalls.push([j * Constants.CELL_WIDTH, i * Constants.CELL_HEIGHT])
      }
    }
    return mazeWalls
  }

  const walls = getMazeWalls()

  useEffect(()=>{
    movePlayer(gyroscopeData.x, gyroscopeData.y);
  }, [gyroscopeData])

  return (
    <Panel id={id}>
      <Div style={{margin: 10, position:'absolute', left: 20, top: 400}}>
        <Text>
          Gyroscope data:
          x={gyroscopeData.x + '\t'}
          y={gyroscopeData.y + '\t'}
          z={gyroscopeData.z}
        </Text>
        <Text>
          Position:
        </Text>
        <Text>
          x={position[0]}{'\t'}
        </Text>
        <Text>
          y={position[1]}{'\t'}
        </Text>
        <br/>
        <Text>
          x={currentX}
        </Text>
        <Text>
          y={currentY}
        </Text>
      </Div>

      <Div style={{margin: 0, width: 100, height: 100, padding: 0}}>
        {/*{Maze Building...}*/}
        {walls.map((w) => {
          return (
            <Div
              key={w[0] * 100 + w[1]}
              style={{
                position: "fixed",
                padding:"0",
                width: Constants.CELL_WIDTH,
                height: Constants.CELL_HEIGHT,
                left: w[0],
                top: w[1],
                backgroundColor: Colors.black,
              }}
            />)
        })}
      </Div>

      {/*{Player}*/}
      <Div style={{
        position: "absolute",
        padding: 2,
        width: Constants.PLAYER_SIZE,
        height: Constants.PLAYER_SIZE,
        left: position[0] + currentX,
        top: position[1] + currentY,
        backgroundColor: Colors.green
      }}/>
    </Panel>
  );
}

export default Maze;
