import React, { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import '@vkontakte/vkui/dist/vkui.css';
import { View, AdaptivityProvider, AppRoot } from '@vkontakte/vkui';
import Maze from './panels/Maze';
import {Gyroscope} from "./constants/Gyroscope";

const App = () => {
  const [activePanel, setActivePanel] = useState('maze');

  const [position, setPosition] = useState([0, 0]);
  const [gyroscope, setGyroscope] = useState({x:0,y:0,z:0});

  const [speedY, setSpeedY] = useState(0);
  const [speedX, setSpeedX] = useState(0);

  const getPosition = (gyrData) => {
    const newSpeedX = speedX + gyrData.y * Gyroscope.force + 30;
    const newSpeedY = speedY + gyrData.x * Gyroscope.force;

    setSpeedX(newSpeedX);
    setSpeedY(newSpeedY);

    return [newSpeedX, newSpeedY];
  }

  useEffect(() => {
    bridge.subscribe(({ detail: { type, data }}) => {
      console.log(type)
      if (type === 'VKWebAppUpdateConfig') {
        const schemeAttribute = document.createAttribute('scheme');
        schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
        document.body.attributes.setNamedItem(schemeAttribute);
      } else if (type === 'VKWebAppGyroscopeChanged') {
        setGyroscope({x:data.x, y:data.y, z:data.z})
        let p = getPosition(data)
        setPosition(p);
      }
    });

    const SubscribeOnGyro = async () => {
      bridge.send("VKWebAppGyroscopeStart", {"refresh_rate": 60}).then(json => {
        console.log(json);
      });
    }

    const SetGyroData = async () => {
      setInterval(() => {
        setPosition(getPosition({x:speedX, y:speedY}))
      }, 70)
    }

    SubscribeOnGyro();
    SetGyroData();
  }, []);

  return (
    <AdaptivityProvider>
      <AppRoot>
        <View activePanel={activePanel}>
          <Maze id='maze' position={position} gyroscopeData={gyroscope}/>
        </View>
      </AppRoot>
    </AdaptivityProvider>
  );
}

export default App;
